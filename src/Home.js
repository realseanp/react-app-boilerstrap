var React = require('react');

var Home = React.createClass({
    render: function(){
        var text = "Welcome to the React bootstrap. Routes included. If you've made it this far you did it right."
        return (
            <p>{text}</p>
        )
    }
});

module.exports = Home;