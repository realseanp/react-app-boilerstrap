var React = require('react');
var Link = require('react-router').Link;

var Links = React.createClass({
    render: function(){
        var links = this.props.children.map(function(el, index){
            return (<li key={index}>{el}</li>)
        });
        return (
            <ul>
                {links}
            </ul>
        )
    }
});

var Navigation = React.createClass({
    render: function(){
        return (
            <ul>
                <Links>
                    <Link to="/">Home</Link>
                    <Link to="/about/">About</Link>
                    <Link to="/inbox/">Inbox</Link>
                </Links>
            </ul>
        )
    }
});

module.exports = Navigation;