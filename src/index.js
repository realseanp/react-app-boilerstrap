var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var Link = Router.Link;

var About = require('./About');
var Inbox = require('./Inbox');
var Home = require('./Home');
var Nav = require('./Nav');

var App = React.createClass({
    render: function(){
      return (
        <div>
            <Nav />
            <h1>App</h1>
            <RouteHandler/>
        </div>
      )
    }
});

var routes = (
  <Route handler={App}>
    <Route path="/" handler={Home} />
    <Route path="/about/" handler={About} />
    <Route path="/inbox/" handler={Inbox} />
  </Route>
);

Router.run(routes, Router.HashLocation, function(Root) {
    React.render(<Root/>, document.getElementById('app'));
});