module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    browserify: {
      dist: {
        files: {
          'build/bundle.js': ['./src/*.js'],
        },
        options: {
          transform: ['reactify']
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          './css/main.css': './src/scss/main.scss'
        }
      }
    },
    uglify: {
      my_target: {
        files: {
          'public/site.min.js': ['build/bundle.js']
        }
      }
    },
    watch: {
      scripts: {
        files: './src/*.js',
        tasks: ['build'],
      },
      css: {
        files: './src/**/*.scss',
        tasks: ['sass']
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['sass']);
  grunt.registerTask('build', ['browserify', 'uglify']);

};