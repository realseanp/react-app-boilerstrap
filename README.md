# React Boilerplate with Routes

This is a boilerplate to quickly get started creating a small React JS app. I am using [React Router](http://rackt.github.io/react-router/) for routes. 

Install dependencies with **npm install**

Run **grunt watch**
